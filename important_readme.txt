NOTE:

Salut William, j'ai tenté de faire le projet de manière purement basique en essayant d'inclure le plus possible les critères requis.

Seulement le thème (avec Material-UI) et le changement de langue ne sont pas fait. Material-ui me prend beaucoup trop de temps à comprendre le fonctionnement
et le changement de langue que j'ai essayé d'implémenter avec une librairie / package appelé "i18next" ne fonctionne pas au final, même après avoir tout essayé
et sa documentation n'est pas exactement facile à comprendre non plus.

Cependant les plus gros points principaux, dont les (minimum) 3 écrans/pages, ainsi que le localstorage (incluant d'ailleur l'utilisation de props et state)
sont implémentéd en un système simple permettant d'ajouter et gérer des éléments dans un tableau, se sauvegardant en localstorage, que j'ai implémenter en me 
servant de guides et documentations pour au moins démontrer que j'ai compris comment utiliser ces derniers critères. Le système se trouve dans la page "Games".

Ma note étant déja très haute dans le cours, celà devrait me suffir pour le passer. Or, j'ai dû transférer mon temps et mon effort dans un autre projet
en cours de programmation Laravel qui est 2 fois plus gros et complexe.

Je prévois refaire l'entièreté et créer mon portfolio dans mon temps libre très bientôt. Et à mon avis, pour futur référence et par critique constructive, je 
crois (de manière subjective) qu'il est préférable pour un étudiant de concevoir son site web portfolio dans son propre temps libre de la manière qu'il souhaite, 
plutôt qu'en évaluation. Ce qui lui laisse la possibilité de s'inspirer et s'exprimer pleinement sans le stress d'avoir à respecter certaines contraintes ou 
deadlines (surtout en fin de session).

Mais bon pour ce dernier point c'est toi qui vois au final, ce n'est que mon avis lors qu'il s'agit d'idée de projets à évaluer.

Merci pour ta compréhension.