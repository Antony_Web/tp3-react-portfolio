import React, { useContext } from "react";
import GameForm from "../components/game_components/GameForm";
import GameList from "../components/game_components/GameList";
import { GameContext }  from "../contexts/GamesContext";

const Games = () => {

    const { games } = useContext(GameContext)

    return ( 
        <main>
                <section className="container mt-5 text-center" style={{width: "600px", border: "solid #332438 10px", borderRadius: "20px", padding: "30px", }}>
                        <div style={{backgroundColor: "#dbdbdb", padding: "20px", borderRadius: "20px"}}>
                            <h1 className="fw-bolder">My gaming list</h1>
                            <h5 className="mt-3">You have { games.length } games to finish.</h5>
                        </div>

                        <div>
                        <GameList />
                        <GameForm />
                            
                        </div>
                </section>
        </main>
     );
}
 
export default Games;