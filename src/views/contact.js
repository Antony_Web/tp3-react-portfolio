import React from "react";
import emailjs from "emailjs-com";
import { useTranslation } from "react-i18next";
import "../translations/i18n";

const Contact = () => {

    function sendEmail(e) {
        e.preventDefault();

        emailjs.sendForm('service_o1te2gf', 'template_b73dsd8', e.target, 'user_PzlBk85PVbOK8vAFfD6CZ').then(res=> {
            console.log(res);
        }).catch(err=> console.log(err));
    }

    const { t } = useTranslation();

    return ( 
        <main>
            <form onSubmit={sendEmail} name="portfolio">
                <div class="mb-3">
                    <label for="name" class="form-label">{t("Your name")}</label>
                    <input type="text" name="name" class="form-control" id="name" required minlength="10"></input>
                    <label className="form-text">{t("Enter your first and last Name")}</label>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">{t("Your email")}</label>
                    <input type="email" name="email" class="form-control" id="email" required></input>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="number">{t("Your phone number")}</label>
                    <input type="tel" name="number" class="form-control" id="number" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required></input>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="message">{t("Leave a message")}</label>
                    <textarea className="form-control" id="message" name="message" rows="5" minlength="50" required></textarea>
                    <label className="form-text">{t("Please, write at least a few words!")}</label>
                </div>

                <button type="submit" class="btn btn-primary">{t("Submit")}</button>
            </form>
        </main>
     );
}
 
export default Contact;