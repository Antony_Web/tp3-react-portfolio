import { waitForElementToBeRemoved } from "@testing-library/dom";
import React, { Component } from "react";
import { useTranslation, Trans } from "react-i18next";
import "../translations/i18n";

const langs = {
    en: { nativeName: 'English'},
    fr: { nativeName: 'Français'}
}


const Navbar = () => {

    const { t, i18n } = useTranslation();

        return ( 
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Antony Lacerte</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="/">{t('Home')}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">{t('About')}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/games">{t('Games')}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/portfolio">{t('Portfolio')}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact">{t('Contact')}</a>
                        </li>
                        <li class="nav-item">
                            {Object.keys(langs).map((lang) => (
                            <button className="btn btn-primary" key={lang} style={{ fontWeight: i18n.resolvedLanguage === lang ? 'bold' : 'normal' }} type="submit" onClick={() => i18n.changeLanguage(lang)}>
                                {langs[lang].nativeName}
                            </button>
                            ))}
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
         );

}
 
export default Navbar;


