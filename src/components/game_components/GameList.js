import React, { useContext } from "react";
import { GameContext } from "../../contexts/GamesContext";
import GameDetails from "./GameDetails";

const GameList = () => {

    const { games } = useContext(GameContext)

    return games.length ? ( // Liste non-organisé contenant les objets du contexte.
        <div>
            <ul>
                { games.map(game => {
                    return ( <GameDetails game={game} key={game.id}/> )
                })}
            </ul>
        </div>
     ) :
     <div>Congratulations, you've gone through your entire list!</div>
}
 
export default GameList;