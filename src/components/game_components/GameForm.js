import React, { useContext, useState } from "react";
import { GameContext } from "../../contexts/GamesContext";

const GameForm = () => {

    const { dispatch } = useContext(GameContext)
    const [title, setTitle] = useState('')
    const [company, setCompany] = useState('')

    const handleSubmit = (e) => { //Handler pour le formulaire lors de la soumission d'un objet dans le tableau.
        e.preventDefault();
        console.log(title, company)
        dispatch({type: 'ADD_GAME', game: {
            title, company
        }})
        setTitle('')
        setCompany('')
    }

    return (   // Le formulaire en question pour ajouter un objet au tableau.
        <form className="mt-4" onSubmit={handleSubmit}>
            <input className="text-center form-control my-3" type="text" id="title" name="title" value={title} placeholder="Insert book title here" onChange={(e) => setTitle(e.target.value)} required></input>
            <input className="text-center form-control my-3" type="text" id="company" name="company" value={company} placeholder="Insert company name here" onChange={(e) => setCompany(e.target.value)} required></input>
            <button className="text-center mt-4 btn btn-success">Add game</button>
        </form>
     );
}
 
export default GameForm;