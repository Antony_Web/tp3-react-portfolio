import React, { useContext } from 'react';
import { GameContext } from '../../contexts/GamesContext';

const GameDetails = ({ game })  => {

    const { dispatch } = useContext(GameContext); //retournera un espace contenant les informations de l'objet ajouté.

    return ( 
        <li className="row text-start my-3" style={{margin: "0", backgroundColor: "#e0e0e0", padding: "20px", borderRadius: "10px"}}>
            <div className="col-10">
                <h5 className="title">{ game.title }</h5>
                <h6 className="company">{ game.company }</h6>
            </div>
            <button className="btn btn-danger col-2" onClick={() => dispatch({type: 'REMOVE_GAME', id: game.id})}>Delete</button>
        </li>
     );
}
 
export default GameDetails;