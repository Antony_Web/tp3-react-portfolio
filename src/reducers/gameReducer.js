import React from 'react'
import {v1 as uuid } from 'uuid'

// Reducer permettant de simplifier la procédure de création d'un objet à enregistrer dans le tableau.

export const gameReducer = (state, action) => {
    switch(action.type) {
        case 'ADD_GAME':
            return[...state, {
                title: action.game.title,
                company: action.game.company,
                id: uuid()
            }]
        case 'REMOVE_GAME':
            return state.filter(game => game.id !== action.id)
        default:
            return state
    }
}