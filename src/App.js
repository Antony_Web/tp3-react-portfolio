import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/navbar";
import About from "./views/about";
import Games from "./views/games";
import Contact from "./views/contact";
import Homepage from "./views/homepage";
import Portfolio from "./views/portfolio";
import GameContextProvider from "./contexts/GamesContext"; // Inclue le contexte dans la page désiré pour pouvoir utiliser et référer ses fonctions.

function App() {
  return (
    <Router>
      <Navbar />
    <Switch>
      <Route exact path="/">
        <Homepage />
      </Route>
      <Route exact path="/about">
        <About/>
      </Route>
      <Route exact path="/games">
        <GameContextProvider>
          <Games/>
        </GameContextProvider>
      </Route>
      <Route exact path="/portfolio">
        <Portfolio/>
      </Route>
      <Route exact path="/contact">
        <Contact />
      </Route>
    </Switch>
    </Router>
    
  );
}

export default App;
