import React, { createContext, useReducer, useEffect } from "react";
import { gameReducer } from "../reducers/gameReducer";

export const GameContext = createContext()

const GameContextProvider = (props) => { // Ceci va créer un contexte pour référer les données locales du browser qui seront retenus avec ce système.
    const [games, dispatch] = useReducer(gameReducer, [], () => {
        const localData = localStorage.getItem('games');
        return localData ? JSON.parse(localData) : []
    })
    useEffect(() => {
        localStorage.setItem('games', JSON.stringify(games))
    }, [games])
    
    return ( // retournera les valeurs enfants implémentés par les autres components relié à ce contexte.
        <GameContext.Provider value={{games, dispatch}}>
            {props.children}
        </GameContext.Provider>
    )
}

export default GameContextProvider